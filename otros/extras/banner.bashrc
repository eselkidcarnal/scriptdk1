#!/bin/bash
source msg
terminal_width="$(stty size | cut -d" " -f2)"
#if [[ "$terminal_width" =~ ^[0-9]+$ ]] && [ "$terminal_width" -gt 60 ]; then

    motd="
 \e[47m                \e[0m  $(printext 'ＣｈｕＫＫ－ＳＣＲＩＰＴ')\e[0m
 \e[47m  \e[0m            \e[0;37m\e[47m .\e[0m
 \e[47m  \e[0m  \e[47m  \e[0m        \e[47m  \e[0m  \e[1m\e[1;33mSoporte:\e[0m \e[4m\e[1;97mhttps://t.me/drowkid01\e[0m
 \e[47m  \e[0m  \e[47m  \e[0m        \e[47m  \e[0m  \e[1;33mVersión:\e[0m \e[4m\e[1;97m$(cat /etc/adm-lite/v-local.log)\e[0m
 \e[47m  \e[0m            \e[47m  \e[0m  \e[1;33mReseller:\e[0m \e[4m\e[1;97m$(cat /etc/adm-lite/menu_credito)\e[0m
 \e[47m  \e[0m            \e[0;37m\e[47m .\e[0m
 \e[47m                \e[0m  \e[1;33m$(date +"%d/%m/%Y")-$(date +"%H:%M")
"


echo -e "$motd"
